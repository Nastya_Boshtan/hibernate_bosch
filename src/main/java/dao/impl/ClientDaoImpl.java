package dao.impl;

import com.epam.Client;
import dao.ClientDao;
import dao.Config;
import java.sql.SQLException;
import org.hibernate.Session;
import org.hibernate.query.Query;


public class ClientDaoImpl implements ClientDao {

  Session session = Config.getSession();

  public void findAll() throws SQLException {

    Query query = session.createQuery("from " + "Client");
    for (Object obj : query.list()) {
      Client client = (Client) obj;
      System.out.format("%d %s %s %s %s %s %s %s %s \n", client.getId(),
          client.getSurname(), client.getName(), client.getMiddleName(), client.getCountry(),
          client.getCity(), client.getStreet(), client.getHouseNumber(), client.getPhone());

    }
  }

  public void create() throws SQLException {

      int id = 12;
      String surname = "Sadoha";
      String name = "Karina";
      String middleName = "Volodumurivna";
      String country = "Ukraine";
      String city = "Lviv";
      String street = "Chornovola";
      String houseNumber = "2a";
      String phone = "+380962103457";
      session.beginTransaction();
      Client client = new Client();
      client.setId(id);
      client.setSurname(surname);
      client.setName(name);
      client.setMiddleName(middleName);
      client.setCountry(country);
      client.setCity(city);
      client.setStreet(street);
      client.setHouseNumber(houseNumber);
      client.setPhone(phone);
      session.save(client);
      session.getTransaction().commit();
      System.out.println("Змінена таблиця за рахунок додавання нового запису");
      findAll();

  }

  public void update() {
    try {
      int id = 12;
      String newname = "Yulia";
      Client client =(Client) session.load(Client.class, id);
      if (client != null) {
        session.beginTransaction();
        Query query = session.createQuery

            ("update Client set name=:value1 where name = :value2");

        query.setParameter("value1", newname);
        query.setParameter("value2", id);
        query.executeUpdate();
        session.getTransaction().commit();
        }
    } finally {
      session.close();
    }
  }
}

