package dao;

import java.sql.SQLException;
import java.util.List;

public interface BaseDao <T, ID> {
  void findAll() throws SQLException;
 void create() throws SQLException;
 void update() throws SQLException;
}
