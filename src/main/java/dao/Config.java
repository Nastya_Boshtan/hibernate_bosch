package dao;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Config {
  private static SessionFactory ourSessionFactory;

  private Config() {}

  public static org.hibernate.Session getSession() throws HibernateException {
    try {
      Configuration configuration = new Configuration();
      configuration.configure();

      ourSessionFactory = configuration.buildSessionFactory();
    } catch (Throwable ex) {
      throw new ExceptionInInitializerError(ex);
    }
    return ourSessionFactory.openSession();
  }
}
