package com.epam;

import java.sql.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Orders {

  private Integer id;
  private String nameEquipment;
  private Date startDate;
  private Date endDate;
  private Integer cash;
  private String status;
  private String model;
  private Worker workerByWorkerId;
  private Equipment equipmentByEquipmentId;
  private Client clientByClientId;
  private Set<Part> parts;
  private Set<TypeOfWork> types;

  @Id
  @Column(name = "id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Basic
  @Column(name = "Name_equipment")
  public String getNameEquipment() {
    return nameEquipment;
  }

  public void setNameEquipment(String nameEquipment) {
    this.nameEquipment = nameEquipment;
  }

  @Basic
  @Column(name = "Start_date")
  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  @Basic
  @Column(name = "End_date")
  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  @Basic
  @Column(name = "Cash")
  public Integer getCash() {
    return cash;
  }

  public void setCash(Integer cash) {
    this.cash = cash;
  }

  @Basic
  @Column(name = "Status")
  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @Basic
  @Column(name = "Model")
  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Orders orders = (Orders) o;

    if (id != null ? !id.equals(orders.id) : orders.id != null) {
      return false;
    }
    if (nameEquipment != null ? !nameEquipment.equals(orders.nameEquipment)
        : orders.nameEquipment != null) {
      return false;
    }
    if (startDate != null ? !startDate.equals(orders.startDate) : orders.startDate != null) {
      return false;
    }
    if (endDate != null ? !endDate.equals(orders.endDate) : orders.endDate != null) {
      return false;
    }
    if (cash != null ? !cash.equals(orders.cash) : orders.cash != null) {
      return false;
    }
    if (status != null ? !status.equals(orders.status) : orders.status != null) {
      return false;
    }
    if (model != null ? !model.equals(orders.model) : orders.model != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (nameEquipment != null ? nameEquipment.hashCode() : 0);
    result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
    result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
    result = 31 * result + (cash != null ? cash.hashCode() : 0);
    result = 31 * result + (status != null ? status.hashCode() : 0);
    result = 31 * result + (model != null ? model.hashCode() : 0);
    return result;
  }

  @ManyToOne
  @JoinColumn(name = "Worker_id", referencedColumnName = "id", nullable = false)
  public Worker getWorkerByWorkerId() {
    return workerByWorkerId;
  }

  public void setWorkerByWorkerId(Worker workerByWorkerId) {
    this.workerByWorkerId = workerByWorkerId;
  }

  @ManyToOne
  @JoinColumn(name = "Equipment_id", referencedColumnName = "id", nullable = false)
  public Equipment getEquipmentByEquipmentId() {
    return equipmentByEquipmentId;
  }

  public void setEquipmentByEquipmentId(Equipment equipmentByEquipmentId) {
    this.equipmentByEquipmentId = equipmentByEquipmentId;
  }

  @ManyToOne
  @JoinColumn(name = "Client_id", referencedColumnName = "id", nullable = false)
  public Client getClientByClientId() {
    return clientByClientId;
  }

  public void setClientByClientId(Client clientByClientId) {
    this.clientByClientId = clientByClientId;
  }

  @ManyToMany
  @JoinTable(name = "order_has_part", catalog = "", schema = "bosch", joinColumns = @JoinColumn(name = "Order_id", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "Part_Name", referencedColumnName = "Name", nullable = false))
  public Set<Part> getParts() {
    return parts;
  }

  public void setParts(Set<Part> parts) {
    this.parts = parts;
  }

  @ManyToMany(mappedBy = "ordersId")
  public Set<TypeOfWork> getTypes() {
    return types;
  }

  public void setTypes(Set<TypeOfWork> types) {
    this.types = types;
  }


  @Override
  public String toString() {
    return "Orders{" +
        "id=" + id +
        ", nameEquipment='" + nameEquipment + '\'' +
        ", startDate=" + startDate +
        ", endDate=" + endDate +
        ", cash=" + cash +
        ", status='" + status + '\'' +
        ", model='" + model + '\'' +
        ", workerByWorkerId=" + workerByWorkerId +
        ", equipmentByEquipmentId=" + equipmentByEquipmentId +
        ", clientByClientId=" + clientByClientId +
        ", parts=" + parts +
        ", types=" + types +
        '}';
  }

}
