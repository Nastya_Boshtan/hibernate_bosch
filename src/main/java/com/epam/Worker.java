package com.epam;

import java.sql.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Worker {


  private Integer id;
  private String surname;
  private String name;
  private String middleName;
  private String city;
  private String street;
  private String houseNumber;
  private String phone;
  private String position;
  private Date startDate;
  private String hourOfWork;
  private Integer salary;

  @Id
  @Column(name = "id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Basic
  @Column(name = "Surname")
  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  @Basic
  @Column(name = "Name")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Basic
  @Column(name = "Middle_name")
  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  @Basic
  @Column(name = "City")
  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  @Basic
  @Column(name = "Street")
  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  @Basic
  @Column(name = "House_number")
  public String getHouseNumber() {
    return houseNumber;
  }

  public void setHouseNumber(String houseNumber) {
    this.houseNumber = houseNumber;
  }

  @Basic
  @Column(name = "Phone")
  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Basic
  @Column(name = "Position")
  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }

  @Basic
  @Column(name = "Start_date")
  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  @Basic
  @Column(name = "Hour_of_work")
  public String getHourOfWork() {
    return hourOfWork;
  }

  public void setHourOfWork(String hourOfWork) {
    this.hourOfWork = hourOfWork;
  }

  @Basic
  @Column(name = "Salary")
  public Integer getSalary() {
    return salary;
  }

  public void setSalary(Integer salary) {
    this.salary = salary;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Worker worker = (Worker) o;

    if (id != null ? !id.equals(worker.id) : worker.id != null) {
      return false;
    }
    if (surname != null ? !surname.equals(worker.surname) : worker.surname != null) {
      return false;
    }
    if (name != null ? !name.equals(worker.name) : worker.name != null) {
      return false;
    }
    if (middleName != null ? !middleName.equals(worker.middleName) : worker.middleName != null) {
      return false;
    }
    if (city != null ? !city.equals(worker.city) : worker.city != null) {
      return false;
    }
    if (street != null ? !street.equals(worker.street) : worker.street != null) {
      return false;
    }
    if (houseNumber != null ? !houseNumber.equals(worker.houseNumber)
        : worker.houseNumber != null) {
      return false;
    }
    if (phone != null ? !phone.equals(worker.phone) : worker.phone != null) {
      return false;
    }
    if (position != null ? !position.equals(worker.position) : worker.position != null) {
      return false;
    }
    if (startDate != null ? !startDate.equals(worker.startDate) : worker.startDate != null) {
      return false;
    }
    if (hourOfWork != null ? !hourOfWork.equals(worker.hourOfWork) : worker.hourOfWork != null) {
      return false;
    }
    if (salary != null ? !salary.equals(worker.salary) : worker.salary != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (surname != null ? surname.hashCode() : 0);
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
    result = 31 * result + (city != null ? city.hashCode() : 0);
    result = 31 * result + (street != null ? street.hashCode() : 0);
    result = 31 * result + (houseNumber != null ? houseNumber.hashCode() : 0);
    result = 31 * result + (phone != null ? phone.hashCode() : 0);
    result = 31 * result + (position != null ? position.hashCode() : 0);
    result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
    result = 31 * result + (hourOfWork != null ? hourOfWork.hashCode() : 0);
    result = 31 * result + (salary != null ? salary.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Worker{" +
        "id=" + id +
        ", surname='" + surname + '\'' +
        ", name='" + name + '\'' +
        ", middleName='" + middleName + '\'' +
        ", city='" + city + '\'' +
        ", street='" + street + '\'' +
        ", houseNumber='" + houseNumber + '\'' +
        ", phone='" + phone + '\'' +
        ", position='" + position + '\'' +
        ", startDate=" + startDate +
        ", hourOfWork='" + hourOfWork + '\'' +
        ", salary=" + salary +
        '}';
  }
}
