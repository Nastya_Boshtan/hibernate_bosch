package com.epam;

import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "type_of_work", schema = "bosch", catalog = "")
public class TypeOfWork {



  private Integer id;
  private String typeOfEquipment;
  private Integer price;
  private Set<Orders> ordersId;

  @Id
  @Column(name = "id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Basic
  @Column(name = "Type_of_equipment")
  public String getTypeOfEquipment() {
    return typeOfEquipment;
  }

  public void setTypeOfEquipment(String typeOfEquipment) {
    this.typeOfEquipment = typeOfEquipment;
  }

  @Basic
  @Column(name = "Price")
  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    TypeOfWork that = (TypeOfWork) o;

    if (id != null ? !id.equals(that.id) : that.id != null) {
      return false;
    }
    if (typeOfEquipment != null ? !typeOfEquipment.equals(that.typeOfEquipment)
        : that.typeOfEquipment != null) {
      return false;
    }
    if (price != null ? !price.equals(that.price) : that.price != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (typeOfEquipment != null ? typeOfEquipment.hashCode() : 0);
    result = 31 * result + (price != null ? price.hashCode() : 0);
    return result;
  }

  @ManyToMany
  @JoinTable(name = "type_of_work_has_order", catalog = "", schema = "bosch", joinColumns = @JoinColumn(name = "Type_of_work_id", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "Order_id", referencedColumnName = "id", nullable = false))
  public Set<Orders> getOrdersId() {
    return ordersId;
  }

  public void setOrdersId(Set<Orders> ordersId) {
    this.ordersId = ordersId;
  }
  @Override
  public String toString() {
    return "TypeOfWork{" +
        "id=" + id +
        ", typeOfEquipment='" + typeOfEquipment + '\'' +
        ", price=" + price +
        ", ordersId=" + ordersId +
        '}';
  }
}
