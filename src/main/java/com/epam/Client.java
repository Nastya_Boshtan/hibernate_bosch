package com.epam;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Client {

  public Integer id;
  public String surname;
  public String name;
  public String middleName;
  public String country;
  public String city;
  public String street;
  public String houseNumber;
  public String phone;

  @Id
  @Column(name = "id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Basic
  @Column(name = "Surname")
  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  @Basic
  @Column(name = "Name")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Basic
  @Column(name = "Middle_name")
  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  @Basic
  @Column(name = "Country")
  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  @Basic
  @Column(name = "City")
  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  @Basic
  @Column(name = "Street")
  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  @Basic
  @Column(name = "House_number")
  public String getHouseNumber() {
    return houseNumber;
  }

  public void setHouseNumber(String houseNumber) {
    this.houseNumber = houseNumber;
  }

  @Basic
  @Column(name = "Phone")
  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Client client = (Client) o;

    if (id != null ? !id.equals(client.id) : client.id != null) {
      return false;
    }
    if (surname != null ? !surname.equals(client.surname) : client.surname != null) {
      return false;
    }
    if (name != null ? !name.equals(client.name) : client.name != null) {
      return false;
    }
    if (middleName != null ? !middleName.equals(client.middleName) : client.middleName != null) {
      return false;
    }
    if (country != null ? !country.equals(client.country) : client.country != null) {
      return false;
    }
    if (city != null ? !city.equals(client.city) : client.city != null) {
      return false;
    }
    if (street != null ? !street.equals(client.street) : client.street != null) {
      return false;
    }
    if (houseNumber != null ? !houseNumber.equals(client.houseNumber)
        : client.houseNumber != null) {
      return false;
    }
    if (phone != null ? !phone.equals(client.phone) : client.phone != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (surname != null ? surname.hashCode() : 0);
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
    result = 31 * result + (country != null ? country.hashCode() : 0);
    result = 31 * result + (city != null ? city.hashCode() : 0);
    result = 31 * result + (street != null ? street.hashCode() : 0);
    result = 31 * result + (houseNumber != null ? houseNumber.hashCode() : 0);
    result = 31 * result + (phone != null ? phone.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Client{"   +
        "id=" + id +
        ", surname='" + surname + '\'' +
        ", name='" + name + '\'' +
        ", middleName='" + middleName + '\'' +
        ", country='" + country + '\'' +
        ", city='" + city + '\'' +
        ", street='" + street + '\'' +
        ", houseNumber='" + houseNumber + '\'' +
        ", phone='" + phone + '\'' +
        '}';
  }
}
