package com.epam;

import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Part {
  private String name;
  private Integer price;
  private Producer producerByProducerId;
  private Set<Orders> ordersId;

  @Id
  @Column(name = "Name")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Basic
  @Column(name = "Price")
  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Part part = (Part) o;

    if (name != null ? !name.equals(part.name) : part.name != null) {
      return false;
    }
    if (price != null ? !price.equals(part.price) : part.price != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = name != null ? name.hashCode() : 0;
    result = 31 * result + (price != null ? price.hashCode() : 0);
    return result;
  }

  @ManyToOne
  @JoinColumn(name = "Producer_id", referencedColumnName = "id", nullable = false)
  public Producer getProducerByProducerId() {
    return producerByProducerId;
  }

  public void setProducerByProducerId(Producer producerByProducerId) {
    this.producerByProducerId = producerByProducerId;
  }

  @Override
  public String toString() {
    return "Part{" +
        "name='" + name + '\'' +
        ", price=" + price +
        ", producerByProducerId=" + producerByProducerId +
        ", ordersId=" + ordersId +
        '}';
  }

  @ManyToMany(mappedBy = "parts")
  public Set<Orders> getOrdersId() {
    return ordersId;
  }

  public void setOrdersId(Set<Orders> ordersId) {
    this.ordersId = ordersId;
  }
}
