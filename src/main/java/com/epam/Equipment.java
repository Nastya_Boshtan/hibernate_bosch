package com.epam;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Equipment {
  private Integer id;
  private String name;
  private Producer producerByProducerId;

  @Id
  @Column(name = "id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Basic
  @Column(name = "Name")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Equipment equipment = (Equipment) o;

    if (id != null ? !id.equals(equipment.id) : equipment.id != null) {
      return false;
    }
    if (name != null ? !name.equals(equipment.name) : equipment.name != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    return result;
  }

  @ManyToOne
  @JoinColumn(name = "Producer_id", referencedColumnName = "id", nullable = false)
  public Producer getProducerByProducerId() {
    return producerByProducerId;
  }

  public void setProducerByProducerId(Producer producerByProducerId) {
    this.producerByProducerId = producerByProducerId;
  }

  @Override
  public String toString() {
    return "Equipment{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", producerByProducerId=" + producerByProducerId +
        '}';
  }
}
